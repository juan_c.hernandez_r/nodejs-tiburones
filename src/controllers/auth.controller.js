import User from "../models/User";
import config from "../config";
import jwt from "jsonwebtoken";
import Role from "../models/Role";

export const signUp = async (req, res) => {
  let { name, lastName, email, password, role } = req.body;

  if (!password) {
    password = "123abc*sharksclub";
  }

  if (!role) {
    const foundRole = await Role.findOne({ code: "athlete" });
    role = foundRole._id;
  }
  const newUser = await new User({
    isActive: true,
    name,
    lastName,
    email,
    password: await User.encryptPassword(password),
    role,
  });

  await newUser.save();

  res.status(200).json({ message: "success" });
};

export const signIn = async (req, res) => {
  const { email, password } = req.body;
  const userFound = await User.findOne({
    email: email,
  }).populate("role");

  if (!userFound) return res.status(400).json({ message: "User not found" });

  const matchPassword = await User.comparePassword(
    password,
    userFound.password
  );

  if (!matchPassword)
    return res.status(401).json({ token: null, message: "Invalid password" });

  let exp = new Date().getTime();
  const token = jwt.sign({ id: userFound._id }, config.SECRET, {
    expiresIn: exp,
  });
  res.json({ token, user: userFound });
};

function checkPassCode(passCode, userId) {
  if (passCode && userId) {
    return User.findOne({ _id: userId })
      .then((user) => {
        if (user) {
          if (user.checkPassCode(passCode)) {
            if (user.checkPassCodeDate()) {
              return user;
            } else {
              return {
                code: "unauthorized",
                message: "Expired password code",
              };
            }
          } else {
            return {
              code: "unauthorized",
              message: "Invalid password code",
            };
          }
        } else {
          return {
            code: "unauthorized",
            message: "User not found",
          };
        }
      })
      .catch(() => {
        return {
          code: "unauthorized",
          message: "User not found",
        };
      });
  } else {
    return {
      code: "unauthorized",
      message: "Invalid request",
    };
  }
}

exports.checkPassCode = function (req, res) {
  const passCode = req.body.passwordCode;
  const userId = req.body.userId;
  return checkPassCode(passCode, userId)
    .then((response) => {
      if (response._id) {
        res.json({
          success: true,
          message: "Valid password code",
        });
      } else {
        res.status(500).json(response);
      }
    })
    .catch((error) => {
      res.status(500).json(error.message);
    });
};

export const updatePassword = async (req, res) => {
  const encriptPassword = await User.encryptPassword(req.body.password);

  if (req.body.userId) {
    return checkPassCode(req.body.passwordCode, req.body.userId)
      .then((user) => {
        if (req.body.password === req.body.passwordConfirm) {
          user.password = encriptPassword;
          user.newPasswordCode = null;
          user.newPasswordCodeDate = null;
          user.save();
          return res.json({
            success: true,
            result: [],
            message: "New password created.",
          });
        } else {
          throw {
            code: 500,
            message: "Las contraseñas no coinciden",
          };
        }
      })
      .catch((err) => {
        return res.status(500).json({
          success: false,
          result: [],
          message: err.message,
        });
      });
  } else {
    return res.status(500).json({
      success: false,
      result: [],
      message: "userId es requerido",
    });
  }
};
