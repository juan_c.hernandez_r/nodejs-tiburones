import User from "../models/User";
import Role from "../models/Role";
export const createUser = (req, res) => {
  res.json("Creating user");
};
export const getUsers = async (req, res) => {
  const users = await User.find({ isDeleted: false, isActive: true })
    .select("name lastName email  isActive")
    .sort({ _id: -1 });
  res.json(users);
};

export const getRoles = async (req, res) => {
  const roles = await Role.find({ isDeleted: false, isActive: true }).select(
    "name "
  );
  res.json(roles);
};

export const getUserById = async (req, res) => {
  const { userId } = req.params;
  const user = await User.findById(userId);
  res.status(200).json(user);
};

export const updateUserById = async (req, res) => {
  const { userId } = req.params;
  const updatedUser = await User.findByIdAndUpdate(userId, req.body);
  res.status(200).json(updatedUser);
};
export const activeUserById = async (req, res) => {
  const { userId } = req.params;
  const { isActive } = req.body;
  const updatedUser = await User.findByIdAndUpdate(userId, {
    isActive: isActive,
  });
  res.status(200).json(updatedUser);
};
export const deleteUserById = async (req, res) => {
  const { userId } = req.params;
  const user = await User.findByIdAndUpdate(userId, {
    isDeleted: true,
    isActive: false,
  });
  res.status(204).json(user);
};
