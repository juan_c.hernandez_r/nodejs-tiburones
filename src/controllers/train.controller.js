import Train from "../models/Train";

export const createTrain = async (req, res) => {
  const { name, note, date } = req.body;
  const newTrain = new Train({ name, note, date });
  const trainSaved = await newTrain.save();
  res.status(201).json(trainSaved);
};
export const getTrainings = async (req, res) => {
  const trainings = await Train.find({ isDeleted: false, isActive: true });
  res.json(trainings);
};

export const getTrainById = async (req, res) => {
  const { trainId } = req.params;
  const train = await Train.findOne({
    _id: trainId,
    isDeleted: false,
    isActive: true,
  });
  res.status(200).json(train);
};

export const updateTrainById = async (req, res) => {
  const { trainId } = req.params;
  const updatedTrain = await Train.findByIdAndUpdate(trainId, req.body);
  res.status(200).json(updatedTrain);
};

export const deleteTrainById = async (req, res) => {
  const { trainId } = req.params;
  const updatedTrain = await Train.findByIdAndUpdate(trainId, {
    isDeleted: true,
    isActive: false,
  });
  res.status(200).json(updatedTrain);
};

export const addUserToTrain = async (req, res) => {
  const { trainId } = req.params;
  const updatedTrain = await Train.findByIdAndUpdate(trainId, {
    $push: { athletes: req.body },
  });
  res.status(200).json(updatedTrain);
};

export const getUsersByTrain = async (req, res) => {
  const { trainId } = req.params;
  const train = await Train.findOne({
    _id: trainId,
    isDeleted: false,
    isActive: true,
  });
  res.status(200).json(train.athletes);
};

export const updateMarkTrain = async (req, res) => {
  const { trainId } = req.params;
  const updatedTrain = await Train.findByIdAndUpdate(trainId, {
    athletes: req.body,
  });
  res.status(200).json(updatedTrain);
};
