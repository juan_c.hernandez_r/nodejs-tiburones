import nodemailer from "nodemailer";
import User from "../models/User";

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "turismocultura.col@gmail.com",
    pass: "TurismoCultura*2022/",
  },
});

export const sendForgotEmail = async (req, res) => {
  let email = req.body.email;
  if (email) {
    User.findOne({ email: email })
      .then((user) => {
        if (user) {
          user.genNewPassCode();
          let mailOptions = {
            from: "turismocultura.col@gmail.com",
            to: email,
            subject: "Olvidaste tu contraseña",
            html: `
            <html lang="es">
              <head>
                <meta charset="UTF-8">
                <title>Email</title>
              </head>
              <body>
                <div>
                  <h2>Recupera tu contraseña</h2>
                  <p>
                  Por medio de este link puedes reestablecer tu contraseña
                  <a href="${
                    process.env.FRONT || "http://localhost:3000/"
                  }recuperar-contrasena/${user.newPasswordCode}/${
              user._id
            }">Click aquí</a>
                  </p>
                </div>
              </body>
            </html>`,
          };

          transporter.sendMail(mailOptions, function (err, data) {
            if (err) {
              console.log("Error", err);
            } else {
              console.log("sucess");
            }
          });
          res.json({
            success: true,
            message: "Email Sent Successfully",
          });
        } else {
          res.status(500).json({
            code: "email_not_found",
            message: "We could not find this email. Please try again.",
          });
        }
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({
          code: "internal_server_error",
          message: "Error consulting user",
        });
      });
  }
};
