import express from "express";
import morgan from "morgan";
const app = express();
import trainRoutes from "./routes/train.routes";
import authRoutes from "./routes/auth.routes";
import usersRoutes from "./routes/user.routes";

app.use(morgan("dev"));
app.use(express.json());
const cors = require("cors");
let allowedOrigins = ["http://localhost:3000"];
app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        let msg =
          "The CORS policy for this site does not " +
          "allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  })
);

app.use("/api/trainings", trainRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/users", usersRoutes);

export default app;
