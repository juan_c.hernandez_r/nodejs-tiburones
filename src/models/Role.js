import { Schema, model } from "mongoose";

const RoleSchema = new Schema(
  {
    code: String,
    name: String,
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now, index: true },
    modifiedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

export default model("Role", RoleSchema);
