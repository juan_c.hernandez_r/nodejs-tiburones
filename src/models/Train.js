import { Schema, model } from "mongoose";

const TrainSchema = new Schema(
  {
    trainer: { type: Schema.Types.ObjectId, ref: "User" },
    note: String,
    name: String,
    date: String,
    athletes: [
      {
        id: { type: Schema.Types.ObjectId, ref: "User" },
        name: String,
        mark: String,
      },
    ],
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now, index: true },
    modifiedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

export default model("Train", TrainSchema);
