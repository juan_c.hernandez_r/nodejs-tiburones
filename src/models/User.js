import { Schema, model } from "mongoose";
import utils from "../helpers/utils";
import bcrypt from "bcryptjs";

const activationCodeExpiration = 24;

const UserSchema = new Schema(
  {
    name: String,
    lastName: String,
    role: { ref: "Role", type: Schema.Types.ObjectId },
    email: { type: String, unique: true },
    password: { type: String, required: true },
    newPasswordCode: { type: String },
    newPasswordCodeDate: { type: Date },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now, index: true },
    modifiedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

UserSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.statics.comparePassword = async (password, receivedPassword) => {
  return await bcrypt.compare(password, receivedPassword);
};

UserSchema.methods.genNewPassCode = function () {
  this.newPasswordCode = utils.uid(128);
  this.newPasswordCodeDate = Date.now();
  this.newPasswordCodeDate.setHours(
    this.newPasswordCodeDate.getHours() + activationCodeExpiration
  );
  return this.save();
};

UserSchema.methods.checkPassCode = function (userPassCode) {
  return this.newPasswordCode !== "" && this.newPasswordCode === userPassCode;
};

UserSchema.methods.checkPassCodeDate = function () {
  let now = Date.now();
  return now < this.newPasswordCodeDate;
};

export default model("User", UserSchema);
