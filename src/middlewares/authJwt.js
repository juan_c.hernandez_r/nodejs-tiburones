import jwt from "jsonwebtoken";
import config from "../config";
import User from "../models/User";
import Role from "../models/Role";

export const verifyToken = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];

    if (!token) return res.status(403).json({ message: "No token provided" });

    const decoded = jwt.verify(token, config.SECRET);
    req.userId = decoded.id;

    const user = await User.findById(req.userId, { password: 0 });
    if (!user) return res.status(404).json({ message: "no user found" });
    next();
  } catch (error) {
    return res.status(401).json({ message: "Unauthorized" });
  }
};

export const isSuperAdmin = async (req, res, next) => {
  const user = await User.findById(req.userId);
  const role = await Role.findById(user.role);
  if (role && role.code === "superadmin") {
    next();
    return;
  }
  return res.status(403).json({ message: "Require super admin role" });
};

/* const values = await Promise.all([
    new Role({ code: "user", name: "Usuario" }).save(),
    new Role({ code: "admin", name: "Administrador" }).save(),
    new Role({ code: "superadmin", name: "Super Admin" }).save(),
    new Role({ code: "seller", name: "Comerciante" }).save(),
  ]);
 */
export const isAdmin = async (req, res, next) => {};

export const isSeller = async (req, res, next) => {};

export const isUser = async (req, res, next) => {};
