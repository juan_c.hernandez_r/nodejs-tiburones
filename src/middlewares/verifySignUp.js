import User from "../models/User";

export const checkDuplicateEmail = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (user) return res.status(400).json({ message: "El usuario ya existe" });

  next();
};

export const checkRoleExisted = (req, res, next) => {
  if (req.body.role) {
    // traer los roles y colocarlos aquí
  }
};
