import { Router } from "express";
import * as authCtrl from "../controllers/auth.controller";
import * as emailCtrl from "../controllers/email.controller";
import { verifySignUp } from "../middlewares";

const router = Router();

router.post("/signup", verifySignUp.checkDuplicateEmail, authCtrl.signUp);
router.post("/signin", authCtrl.signIn);
router.post("/forgotPassword", emailCtrl.sendForgotEmail);
router.post("/checkPassCode", authCtrl.checkPassCode);
router.post("/updatePassword", authCtrl.updatePassword);

export default router;
