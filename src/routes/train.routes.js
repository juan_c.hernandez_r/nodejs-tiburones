import { Router } from "express";
import * as trainCtrl from "../controllers/train.controller";
const router = Router();

router.post("/", trainCtrl.createTrain);
router.get("/", trainCtrl.getTrainings);
router.get("/users/:trainId", trainCtrl.getUsersByTrain);
router.put("/addMark/:trainId", trainCtrl.updateMarkTrain);
router.get("/:trainId", trainCtrl.getTrainById);
router.put("/:trainId", trainCtrl.updateTrainById);
router.delete("/:trainId", trainCtrl.deleteTrainById);
router.put("/addUser/:trainId", trainCtrl.addUserToTrain);

export default router;
