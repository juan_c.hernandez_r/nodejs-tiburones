import { Router } from "express";
import * as userCtrl from "../controllers/user.controller";
import { authJwt } from "../middlewares";
const router = Router();

router.post("/", userCtrl.createUser);
router.get("/", userCtrl.getUsers);
router.get("/roles", userCtrl.getRoles);
//router.get("/rolesRegister", userCtrl.getRolesRegister);
router.get("/:userId/get", userCtrl.getUserById);
router.delete("/:userId/delete", userCtrl.deleteUserById);
router.put("/:userId/edit", userCtrl.updateUserById);
/*router.put("/:userId/active", userCtrl.activeUserById);
 */
export default router;
